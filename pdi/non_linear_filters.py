import matplotlib.pyplot as plt
import numpy as np
from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter import simpledialog

# Converte para escala de cinza, baseada na documentação oficial do numpy
def rgb2gray(rgb):
	return np.dot(rgb[..., :3], [0.2989, 0.5870, 0.1140])

# Função básica que retorna a mediana de uma lista
def get_median(data):
	data = sorted(data)
	n = len(data)
	if n == 0:
		return 0
	if n % 2 == 1:
		return data[n // 2]
	else:
		i = n // 2
		return (data[i - 1] + data[i]) / 2


# Função básica que retorna a media geométrica de uma lista
def get_media_geometrica(data):
	result = np.prod(data)
	result = result ** (1 / len(data))
	return result


def apply_filter_on_image(file, custom_filter, radius=3, remove_absolute_black=True):
	# Lê a imagem
	img = plt.imread(file)

	# Converte para escala de cinza
	img = rgb2gray(img)

	# Adiciona o padding
	img = np.pad(img, pad_width=radius, mode='constant', constant_values=0)

	# Pega a dimensão da imagem em X,Y com padding
	row, col = img.shape

	plt.figure()
	plt.title('Figura original com padding preto', fontsize=16)
	plt.imshow(img, 'gray')
	plt.show()

	img_mod = np.zeros((row, col), dtype=np.uint8)
	for r in range(radius, row - radius):
		for c in range(radius, col - radius):
			sub_matrix = list(img[r - radius:r + radius + radius, c - radius:c + radius + radius].reshape(-1))
			if custom_filter == 0:
				img_mod[r][c] = get_median(sub_matrix)
				msg = "Figura após mediana sem o padding"
			else:
				img_mod[r][c] = get_media_geometrica(sub_matrix)
				msg = "Figura após media geometrica sem o padding"

	img_mod = img_mod[radius:-radius, radius:-radius]
	plt.figure()
	plt.title(msg, fontsize=16)
	plt.imshow(img_mod, 'gray')
	plt.show()


root = Tk()
filter_size = simpledialog.askinteger(title="PDI - Trabalho 1", prompt="Digite o radius do filtro em pixels")
root.withdraw()
filename = askopenfilename()

apply_filter_on_image(filename, 1, filter_size)
apply_filter_on_image(filename, 0, filter_size)
